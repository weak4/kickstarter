module ApplicationHelper

  def wei_to_eth(value)
    number_with_precision(value.to_f/1_000_000_000_000_000_000, precision: 6, significant: true, strip_insignificant_zeros: true)
  end
end

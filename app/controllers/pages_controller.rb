class PagesController < ApplicationController
  before_filter :check_auth, only: [:profile]

  def index
    @projects_by_date = Project.where(status: 'active').order('created_at DESC').group_by{|x| x.created_at.strftime("%d %b. %Y")}
  end

  def profile
    @transactions_by_date = Transaction.where(user_id: current_user.id).order('created_at DESC').group_by{|x| x.created_at.strftime("%d %b. %Y")}
  end
end

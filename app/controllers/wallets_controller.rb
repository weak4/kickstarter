class WalletsController < ApplicationController
  before_filter :check_auth

  def create
    if current_user.ethereum_wallet
      @message = 'You alredy have wallet'
      @close = true
    else
      wallet = EthereumWallet.create(user_id: current_user.id, password: params[:ethereum_wallet][:password])
      if wallet.id
        flash[:notice] = 'Ethereum Wallet created'
        @redirect = true
      else
        @message = wallet.errors[:password].join(', ')
        @close = false
      end
    end
  end
end

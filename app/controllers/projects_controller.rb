class ProjectsController < ApplicationController

  before_filter :check_auth, only: [:back, :create, :new]
  before_filter :find_project, only: [:show, :back, :edit]

  def index
    if params[:category] && (@category = Category.where(id: params[:category]).first)
      @projects_by_date = Project.where(status: 'active', category_id: params[:category]).order('created_at DESC').group_by{|x| x.created_at.strftime("%d %b. %Y")}
    else
      redirect_to root_path
    end
  end

  def show
  end

  def new
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    @project.user_id = current_user.id
    if @project.save
      redirect_to project_path(@project)
    else
      flash[:alert] = @project.errors.full_messages.join(', ')
      render :new
    end
  end

  def edit
  end

  def back
    begin
      @project.contract.donate(current_user, params[:transaction][:password], params[:transaction][:value])
      redirect_to :back, notice: "Thank you for supporting #{@project.name}!";
    rescue Exception => e
      if e.message == 'Message Authentications Codes do not match!'
        redirect_to :back, alert: 'Wallet password is wrong';
      else
        redirect_to :back, alert: e.message
      end
    end
  end

  private

  def find_project
    @project = Project.find(params[:id])
  end

  def project_params
    params.require(:project).permit(:name, :description, :due_date, :goal, :category_id)
  end
end

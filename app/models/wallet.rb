class Wallet < ApplicationRecord
  belongs_to :user

  def balance
  end

  private

  def check_password_complexity(password)
    errors.add(:password, :upper_case) unless /[A-Z]/.match(password)
    errors.add(:password, :lower_case) unless /[a-z]/.match(password)
    errors.add(:password, :numbers) unless /\d/.match(password)
    errors.add(:password, :length) unless password.length >= 8
    !errors.any?
  end
end

class EthereumWallet < Wallet
  before_validation :generate_wallet
  validates :user_id, :address, :key, presence: true

  def balance
    Ethereum::IpcClient.new.get_balance address
  end

  def decrypted_key(password)
    Eth::Key.decrypt self.key, password
  end


  def self.admin_key(password)
    Eth::Key.decrypt Rails.application.secrets.ether_wallet_key, password
  end

  attr_accessor :password

  def generate_wallet
    if check_password_complexity(password) && new_record?
      eth_key = Eth::Key.new
      self.address = eth_key.address # EIP55 checksummed address
      self.key = Eth::Key.encrypt eth_key, password
    end
  end
end

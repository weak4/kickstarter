class Project < ApplicationRecord
  belongs_to :category
  belongs_to :user
  has_one :contract
  has_many :transactions

  validates :name, :description, :goal, :due_date, :user_id, presence: true
  validate :user_balance

  attr_accessor :password

  def pledged
    contract.balance rescue 0
  end

  def backers_count
    transactions.count
  end

  def deploy_contract(password)
    contract = Ethereum::Contract.create(file: Rails.root.join('app','contracts','kickstarter.sol'))
    contract.key = user.ethereum_wallet.decrypted_key(password)
    address = contract.deploy_and_wait(goal)
    Contract.create(project_id: id, address: address)
    
    FinishFundingJob.set(wait_until: due_date.end_of_day).perform_later(id)
  end

  def self.day_color(date)
    case date.to_date.wday
    when 0
      'bg-olive'
    when 1
      'bg-orange'
    when 2
      'bg-purple'
    when 3
      'bg-maroon'
    when 4
      'bg-blue'
    when 5
      'bg-navy'
    else
      'bg-red'
    end 
  end

  def user_balance
    errors.add(:base, 'Insufficient funds to deploy contract.') unless user.ethereum_wallet.try(:balance).to_i >= 10000000000000000
  end
end

class Contract < ApplicationRecord
  belongs_to :project
  has_many :transactions


  def confirmDone(admin_password)
    contract = find_contract_by_address
    contract.key = EthereumWallet.admin_key(admin_password)
    transaction = contract.transact_and_wait.confirm_done()
  end

  def cancelFundings(admin_password)
    contract = find_contract_by_address
    contract.key = EthereumWallet.admin_key(admin_password)
    transaction = contract.transact_and_wait.cancel_fundings()
  end

  def donate(user, password, payment_amount)
    contract = find_contract_by_address
    contract.key = user.ethereum_wallet.decrypted_key(password)
    contract.custom_value = payment_amount.to_i
    transaction = contract.transact_and_wait.donate(user.id)

    Transaction.create(user_id: user.id, value: payment_amount, project_id: project_id, address: transaction.id)
  end

  def balance
    Ethereum::IpcClient.new.get_balance address
  end

  private

  def find_contract_by_address
    Ethereum::Contract.create(file: Rails.root.join('app','contracts','kickstarter.sol'), address: address)
  end
end

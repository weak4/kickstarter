class FinishFundingJob < ApplicationJob
  queue_as :default

  def perform(project_id)
    project = Project.find(project_id)
    return unless project
    if project.pledged >= project.goal
      project.contract.confirmDone(Rails.application.secrets.ether_wallet_admin_password)
    else
      project.contract.cancelFundings(Rails.application.secrets.ether_wallet_admin_password)
    end
  end
end

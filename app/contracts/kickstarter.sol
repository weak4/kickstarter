pragma solidity ^0.4.17;

contract KickstarterContract {

    struct Donation {
        address wallet;
        uint deposit;
        uint siteid;
    }
    Donation[] public donations;
    uint donationscount;

    address constant public systemAddress = 0x6cfB37616B48b95Feb6f71655CDFc1A0c3114fD3;
    uint public goal;
    address public project;

    enum State { Started, Finished, Failed }
    State public state;

    function KickstarterContract(uint _goal) {
        project = msg.sender;
        goal = _goal;
    }

    modifier condition(bool _condition) {
        require(_condition);
        _;
    }

    modifier onlysystem() {
        require(msg.sender == systemAddress);
        _;
    }

    modifier inState(State _state) {
        require(state == _state);
        _;
    }

    modifier positiveValue(uint _value){
        require(_value > 0);
        _;
    }

    event KickstarterDone();
    event KickstarterFailed();


    function donate(uint _siteid) payable
        inState(State.Started)
        positiveValue(msg.value)
    {
        donations.push(Donation({wallet: msg.sender, deposit: msg.value, siteid: _siteid}));
    }


    function confirmDone()
        onlysystem
        inState(State.Started)

    {
        if (this.balance >= goal) {
            KickstarterDone();
            state = State.Finished;
            project.transfer(this.balance);
        } else {
            KickstarterFailed();
            state = State.Failed;

            for(uint i=0;i<donations.length;i++)
            {
                var don = donations[i];
                don.wallet.transfer(don.deposit);
            }
        }
    }


    function cancelFundings()
        onlysystem
        inState(State.Started)
    {
        KickstarterFailed();
        state = State.Failed;

        for(uint i=0;i<donations.length;i++)
        {
            var don = donations[i];
            don.wallet.transfer(don.deposit);
        }
    }

}
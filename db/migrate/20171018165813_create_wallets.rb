class CreateWallets < ActiveRecord::Migration[5.0]
  def change
    create_table :wallets do |t|
      t.belongs_to :user
      t.string "address"
      t.text "key"
      t.timestamps
    end
  end
end

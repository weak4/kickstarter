class AddStyleToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :style, :string
  end
end

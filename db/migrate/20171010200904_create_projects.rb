class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.belongs_to :user
      t.string :name
      t.text :description
      t.bigint :goal
      t.timestamps
    end
  end
end

Rails.application.routes.draw do
  devise_for :users
  root 'pages#index'

  get '/profile' => 'pages#profile'

  resources :projects do
    member do
      post :back
    end
  end

  resources :wallets, only: [:create]

end

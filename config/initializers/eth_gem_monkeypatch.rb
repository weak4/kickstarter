module Ethereum
  class Contract

    attr_accessor :custom_value

    def send_raw_transaction(payload, to = nil)
      Eth.configure { |c| c.chain_id = @client.net_version["result"].to_i }
      @nonce ||= @client.get_nonce(key.address) - 1
      @nonce += 1
      args = {
        from: key.address,
        value: custom_value || 0,
        data: payload,
        nonce: @nonce,
        gas_limit: gas_limit,
        gas_price: gas_price
      }
      args[:to] = to if to
      tx = Eth::Tx.new(args)
      tx.sign key
      @client.eth_send_raw_transaction(tx.hex)["result"]
    end


    def build
      class_name = @name.camelize
      parent = self
      create_function_proxies
      create_event_proxies
      class_methods = Class.new do
        extend Forwardable

        def_delegators :parent, :deploy_payload, :deploy_args, :call_payload, :call_args
        def_delegators :parent, :signed_deploy, :key, :key=
        def_delegators :parent, :gas_limit, :gas_price, :gas_limit=, :gas_price=
        def_delegators :parent, :abi, :deployment, :events
        def_delegators :parent, :estimate, :deploy, :deploy_and_wait
        def_delegators :parent, :address, :address=, :sender, :sender=
        def_delegators :parent, :custom_value, :custom_value=
        def_delegator :parent, :call_raw_proxy, :call_raw
        def_delegator :parent, :call_proxy, :call
        def_delegator :parent, :transact_proxy, :transact
        def_delegator :parent, :transact_and_wait_proxy, :transact_and_wait
        def_delegator :parent, :new_filter_proxy, :new_filter
        def_delegator :parent, :get_filter_logs_proxy, :get_filter_logs
        def_delegator :parent, :get_filter_change_proxy, :get_filter_changes
        define_method :parent do
          parent
        end
      end
      Object.send(:remove_const, class_name) if Object.const_defined?(class_name)
      Object.const_set(class_name, class_methods)
      @class_object = class_methods
    end

  end
end